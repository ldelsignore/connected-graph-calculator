
# Luca Del Signore

# This program calculates the expected minnimum
# number of edges need in a graoh with n vertices
# to make it a connected graph.

import numpy as np
import math
import random
import matplotlib.pyplot as plt

# Randomly adds an edge to the graph by
# choosing a random 0 from the adjecency
# matrix and changing it to a 1.
# @param mat - the list type matrix
# @param k	 - the dimension of the matrix
def addEdge(mat, k):
	
	flag = False

	while flag == False:

		colNum = random.randint(0, k-1)
		rowNum = random.randint(0, k-1)

		if mat[rowNum][colNum] == 0:
			mat[rowNum][colNum] = 1
			flag = True	

# Runs singleTrial multiple times and finds
# the average of their sample sizes
def getExpectedValue(size, trials):

	sampleSum = 0.0

	flag = True

	for i in range(trials):
		sample = singleTrial(size)
		sampleSum += sample

	expectedValue = float(sampleSum/trials)

	#print expectedValue
	return expectedValue

def createCoordinateList(mat, size):

	cList = []
	for i in range(size):
		for j in range(size):
			cList.append([i,j])

	return cList

# Adds edges to an initially edgeless
# graph UNTIL the graph becomes connected.
def singleTrial(size):

	# Create all zero matrix
	mat = np.random.randint(0,1,size=(size,size))

	sample = 0

	flag = True

	while flag:
		addEdge(mat, size)
		sample += 1
		if isConnected(mat, size):
			flag = False

	return sample 


# Checks if a given matrix is connected by
# calculating the sum of the number of 1 step
# paths though the number of k step paths
# for every pair of verticies. If this number
# is zero for any pair, then the graph is 
# not connected.
def isConnected(A, k):
	B = A
	
	for i in range(2, k+1):
		B = B + np.linalg.matrix_power(A, i)
	if checkForZeroes(B, k):
		return True
	else:
		return False
	
def checkForZeroes(mat, k):

	for i in range(k):
		for j in range(k):
			if mat[i][j] == 0:
				return False
	return True		


if __name__ == '__main__' :

	xCor = []
	yCor = []

	for k in range(1,30):
		xCor.append(k)
		yCor.append(int(getExpectedValue(k, 8)))

	
	plt.plot(xCor,yCor)
	plt.xlim([-100,100])
	plt.ylim([-100,100])
	plt.show()

